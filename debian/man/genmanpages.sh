#!/bin/bash


txt2man -d "${CHANGELOG_DATE}" -t GRDAB                  -s 1 grdab.txt               > grdab.1
txt2man -d "${CHANGELOG_DATE}" -t SDR-ZMQ-DAEMON         -s 1 sdr-zmq-daemon.txt      > sdr-zmq-daemon.1
